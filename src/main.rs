use serde::{Deserialize, Serialize};
use std::error::Error;

#[macro_use]
extern crate lazy_static;

lazy_static! {
    static ref file_path: std::path::PathBuf =
        directories::ProjectDirs::from("com.gitlab", "ShironCat", "Nhentai Notifications")
            .map_or(std::path::PathBuf::new(), |dirs| {
                dirs.config_dir().to_owned()
            })
            .join("config.toml");
}

#[derive(Default, Deserialize, Serialize, Clone)]
struct Config {
    page: Option<Vec<Page>>,
}

#[derive(Deserialize, Serialize, Clone)]
struct Page {
    url: Option<String>,
    count: Option<u32>,
}

impl Config {
    pub fn load_file() -> Config {
        toml::from_str::<Config>(&std::fs::read_to_string(file_path).unwrap_or_default())
            .unwrap_or_default()
    }
    fn update(&self) -> Result<Config, Box<dyn Error>> {
        Ok(self.clone())
    }
    fn save_file(&self) -> Result<(), Box<dyn Error>> {
        Ok(())
    }
}

// impl Page {
//     fn _get_document(&self) -> Result<nipper::Document, Box<dyn Error>> {
//         Ok(nipper::Document::from(
//             &reqwest::blocking::get(self.url.as_ref().ok_or("no url found".to_owned())?)?.text()?,
//         ))
//     }

//     fn update_count(&mut self) -> Result<(), Box<dyn Error>> {
//         Ok(self.count = Some(
//             self._get_document()?
//                 .select("span.count")
//                 .first()
//                 .text()
//                 .parse::<u32>()?,
//         ))
//     }

//     fn new_updated(&self) -> Page {
//         Page {
//             url: self.url.clone(),
//             count: self.count.clone(),
//         }
//     }
// }

fn config_path() -> std::path::PathBuf {
    match directories::ProjectDirs::from("com.gitlab", "ShironCat", "Nhentai Notifications") {
        Some(project_dirs) => project_dirs.config_dir().to_owned(),
        None => std::path::Path::new("").to_path_buf(),
    }
    .join("config.toml")
}

fn get_document(url: Option<&String>) -> nipper::Document {
    nipper::Document::from(
        &reqwest::blocking::get(url.expect("No URL provided."))
            .expect("Unable to reach page.")
            .text()
            .expect("Unable to retrieve content from page."),
    )
}

fn main() -> Result<(), Box<dyn Error>> {
    Config::load_file().update()?.save_file()?;
    Ok(())
    /*std::fs::write(
        config_path(),
        toml::to_string(&Config {
            page: toml::from_str::<Config>(
                &std::fs::read_to_string(config_path()).unwrap_or_default(),
            )
            .unwrap_or_default()
            .page
            .map(|pages| {
                pages
                    .iter()
                    .filter(|page| page.url.is_some())
                    .map(|page| Page {
                        url: page.url.clone(),
                        count: page
                            .count
                            .map(|count| {
                                get_document(page.url.as_ref())
                                    .select("span.count")
                                    .first()
                                    .text()
                                    .parse::<u32>()
                                    .unwrap_or(count)
                                    - page.count.unwrap_or_default()
                            })
                            .or(Some(255)),
                    })
                    .collect()
            }),
        })
        .unwrap_or_default(),
    )
    .expect("Failed to write config file to disk.");*/
    // toml::from_str::<Config>(&std::fs::read_to_string(
    //     directories::ProjectDirs::from("com.gitlab", "ShironCat", "Nhentai Notifications")
    //         .ok_or(
    //             "no valid home directory path could be retrieved from the operating system"
    //                 .to_owned(),
    //         )?
    //         .config_dir()
    //         .join("config.toml"),
    // )?)?;
    // return Ok(std::fs::write(
    //     directories::ProjectDirs::from("com.gitlab", "ShironCat", "Nhentai Notifications")
    //         .ok_or(
    //             "no valid home directory path could be retrieved from the operating system"
    //                 .to_owned(),
    //         )?
    //         .config_dir()
    //         .join("config.toml"),
    //     toml::to_string(&Config { page: None })?,
    // )?);
    // .get_pages()
    // .ok_or("no pages configured".to_owned())?
    // .iter()
    // .filter(|page: &&Page| page.update_count().is_ok());
    // .map(|page| {
    //     page.get_document()
    //         .ok_or("document not found".to_owned())?
    //         .select("span.count")
    //         .first()
    //         .text()
    //         .parse::<u32>()?
    // })
    // .collect::<Vec<u32>>();
    // toml::from_str::<Config>(
    //     &std::fs::read_to_string(
    //         directories::ProjectDirs::from("com.gitlab", "ShironCat", "Nhentai Notifications")
    //             .unwrap()
    //             .config_dir(),
    //     )
    //     .unwrap(),
    // )
    // .unwrap()
    // .get_pages()
    // .unwrap()
    // .iter()
    // .map(|page| page.get_document().unwrap().select("span.count"));
    // std::fs::write(
    //     "./teste/teste/teste.toml",
    //     toml::to_string(&Config {
    //         page: Some(vec![
    //             Page {
    //                 url: Some("OK".to_owned()),
    //             },
    //             Page {
    //                 url: Some("OK".to_owned()),
    //             },
    //             Page {
    //                 url: Some("OK".to_owned()),
    //             },
    //         ]),
    //     })
    //     .unwrap(),
    // )
    // .unwrap();
    // let config: Config =
    //     toml::from_str(std::fs::read_to_string("./teste/teste/teste.toml")?.as_str())?;
    // println!("OK {:?}", config);
    // if let Some(file) =
    //     directories::ProjectDirs::from("com.gitlab", "ShironCat", "Nhentai Notifications")
    // {
    //     println!("{:?}", file);
    // }
}
